<?php 

namespace Drupal\moc\Controller;
use Drupal\node\Entity\Node;
// use Drupal\user\Entity\User;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Database\Statement;
class EventList
{
  public function EventList ()
  {  
    
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type','event');

    $entity_ids = $query->execute();
    $node = Node::loadMultiple($entity_ids);

return [
  '#theme'=>'eventlist',
  '#items'=> $node,
 
];

  }
}
