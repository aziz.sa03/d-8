<?php 

namespace Drupal\moc\Controller;
use Drupal\node\Entity\Node;
// use Drupal\user\Entity\User;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Database\Statement;
class VisitorList
{
  public function visitorlist ($event_id)
  {  
    
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type','event_registration');
    $query->condition('field_ref_nid_event.target_id', $event_id, '=');
    $entity_ids = $query->execute();
    // kint($entity_ids);
    $node = Node::loadMultiple($entity_ids);

return [
  '#theme'=>'visitorlist',
  '#items'=> $node,
 
];

  }
}
