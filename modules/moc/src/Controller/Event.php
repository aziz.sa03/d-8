<?php 

namespace Drupal\moc\Controller;
use Drupal\node\Entity\Node;
// use Drupal\user\Entity\User;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Database\Statement;
class Event
{
  public function event ()
  {  
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
$items = array();
    $time = time();
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type','event');
    $query->condition('field_time_event.value',$time,'>=');

    $entity_ids = $query->execute();
    $node = Node::loadMultiple($entity_ids);
    

      foreach ($node as $key => $value) {
      if ($value->hasTranslation($language)) {
        $items[]= $value->getTranslation($language); 
    
      }
    
    
    }
    
    
    return [
  '#theme'=>'event',
  '#items'=> $items,
  '#language' => $language,

 
];

  }
}
