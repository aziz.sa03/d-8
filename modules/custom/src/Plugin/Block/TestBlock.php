<?php
namespace Drupal\custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;




/**
 * Provides a block called "Example TestBlock".
 *
 * @Block(
 *  id = "TestBlock",
 *  admin_label = @Translation("TestBlock")
 * )
 */



class TestBlock extends BlockBase
{
  public function build()
  { 
    return [
    '#theme'=>'TestBlock',
    '#items'=>'hi',
    ];
  }
}

