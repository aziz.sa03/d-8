<?php
namespace Drupal\custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
/**
 * Provides a block called "Slider block".
 *
 * @Block(
 *  id = "Slider",
 *  admin_label = @Translation("Slider block")
 * )
 */

class Slider extends BlockBase
{
  public function build()
  {
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type','slider');
    $entity_ids = $query->execute();
    $node = Node::loadMultiple($entity_ids);
  // foreach ($node as $key => $value) {
  //   $users = User::loadMultiple(array($value->uid->target_id));
  //   $flag_link_service = \Drupal::service('flag.link_builder');
  //   $flag_link[$value->nid->value] = $flag_link_service->build('node',$value->nid->value, 'news');
  //   $count[$value->nid->value] = flag_count($value->nid->value);

  // }
    return [
    '#theme'=>'slider',
'#items'=> $node,

    ];
  }
}

