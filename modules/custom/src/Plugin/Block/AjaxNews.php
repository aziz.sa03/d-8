<?php
namespace Drupal\custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
/**
 * Provides a block called "AjaxNews".
 *
 * @Block(
 *  id = "AjaxNews",
 *  admin_label = @Translation("AjaxNews")
 * )
 */

class AjaxNews extends BlockBase
{
  public function build()
  {
    return [
    '#theme'=>'ajax',
 '#items'=> 'dd',

    ];
  }
}

