<?php 

namespace Drupal\custom\Controller;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Database\Statement;
use Symfony\Component\HttpFoundation\Response;
use Drupal\image\Entity\ImageStyle;


class GetAjaxEvent
{


 

  public function get_ajax_event ()
  {  
    

    $tid = $_POST['tid'];
    if (!empty($tid)) {

    $items = array();
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type','news');

  $query->condition('field_news', $tid);

    $entity_ids = $query->execute();
    $node = Node::loadMultiple($entity_ids);
    // kint($node);
  foreach ($node as $key => $value) {
   // kint($value->field_image->view('news'));

    
    $item['nid'] = $value->nid->value;
    $item['title'] = $value->title->value;
    // $item['id_file'] = $value->field_image_news->entity->id();
     $file_path = $value->field_image_news->entity->getFileUri();
    $file_name = $value->field_image_news->entity->getFilename();
    $item['file_path'] = $file_path;
    $item['file_name'] = $file_name;
    $url = ImageStyle::load('news')->buildUrl($file_path);
    $image_style = ImageStyle::load('news');
    $file = \Drupal\file\Entity\File::load($value->field_image_news->entity->id());
    $image_uri = $file->getFileUri();
    $destination_uri = $image_style->buildUri($file->uri->value);
    $new_img = $image_style->createDerivative($image_uri, $destination_uri);
    $item['style_image'] = $url;
    
    // $item['body'] = $value->body->value;
     
    $items[] = $item ;

  }

  
  $response = new Response();
  $response->setContent(json_encode($items,JSON_UNESCAPED_UNICODE));
  $response->headers->set('Content-Type', 'application/json');
  return $response;


  }else{ print 'error'; exit; }}
}
