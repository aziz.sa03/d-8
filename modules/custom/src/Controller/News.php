<?php 

namespace Drupal\custom\Controller;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Database\Statement;
function flag_count($nid)
{
  $query = \Drupal::database()->select('node', 'n');
  $query->fields('n',array('nid'));
  $query->join('flagging', 'f', 'f.entity_id = n.nid');
  $query->condition('f.entity_id', $nid);

$result = $query->execute()->fetchAll();
// dsm($result);
return count($result);
}

class News
{


 

  public function news ()
  {  
    
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type','news');
    $entity_ids = $query->execute();
    $node = Node::loadMultiple($entity_ids);
  foreach ($node as $key => $value) {
    $users = User::loadMultiple(array($value->uid->target_id));
    $flag_link_service = \Drupal::service('flag.link_builder');
    $flag_link[$value->nid->value] = $flag_link_service->build('node',$value->nid->value, 'news');
    $count[$value->nid->value] = flag_count($value->nid->value);

  }
return [
  '#theme'=>'news',
  '#title' => t('News'),
  '#items'=> $node,
  '#username' => $users,
  '#flag' => $flag_link,
  '#count' => $count,
];


  }
}
