<?php

namespace Drupal\custom\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Form\ConfigFormBase;

/**
 * Our custom hero form.
 */
class FormCustom extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return "module_hero_heroform";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $config = $this->config('custom.settings');

    $form['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Lorem ipsum.'),
    );
    $form['advanced']['rival_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rival one'),
      '#default_value' => $config->get('rival_1'),
    ];
    $form['advanced']['rival_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rival one'),
      '#default_value' => $config->get('rival_2'),
    ];
    $form['advanced']['rival_3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rival one'),
      '#default_value' => $config->get('rival_3'),
    ];


    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Lorem ipsum.'),
    );
    $form['settings']['rival_4'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rival one'),
      '#default_value' => $config->get('rival_4'),
    ];


    /** @var TYPE_NAME $form_state */
    return parent::buildForm($form , $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config = $this->configFactory->getEditable('custom.settings');

    $config
    ->set('rival_1' , $form_state->getValue('rival_1'))
    ->save();

    $config
      ->set('rival_2' , $form_state->getValue('rival_2'))
      ->save();

    $config
      ->set('rival_3' , $form_state->getValue('rival_3'))
      ->save();

    $config
      ->set('rival_4' , $form_state->getValue('rival_4'))
      ->save();
    parent::submitForm($form , $form_state);
  }
  protected function getEditableConfigNames(){
    return [

      'custom.settings'
    ];
  }
}

